self: super:
{
  soc-rockchip-Add-rockchip-suspend-mode-driver = {
    name = "soc-rockchip-Add-rockchip-suspend-mode-driver";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-01-soc-rockchip-Add-rockchip-suspend-mode-driver.patch;
  };
  firmware-Add-Rockchip-SIP-driver = {
    name = "firmware-Add-Rockchip-SIP-driver";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-02-firmware-Add-Rockchip-SIP-driver.patch;
  };
  tty-serdev-support-shutdown-op = {
    name = "tty-serdev-support-shutdown-op";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-03-tty-serdev-support-shutdown-op.patch;
  };
  bluetooth-hci_serdev-Clear-registered-bit-on-unregis = {
    name = "bluetooth-hci_serdev-Clear-registered-bit-on-unregis";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-04-bluetooth-hci_serdev-Clear-registered-bit-on-unregis.patch;
  };
  bluetooth-hci_bcm-disable-power-on-shutdown = {
    name = "bluetooth-hci_bcm-disable-power-on-shutdown";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-05-bluetooth-hci_bcm-disable-power-on-shutdown.patch;
  };
  mmc-core-pwrseq_simple-disable-mmc-power-on-shutdown = {
    name = "mmc-core-pwrseq_simple-disable-mmc-power-on-shutdown";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-06-mmc-core-pwrseq_simple-disable-mmc-power-on-shutdown.patch;
  };
  regulator-core-add-generic-suspend-states-support = {
    name = "regulator-core-add-generic-suspend-states-support";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-07-regulator-core-add-generic-suspend-states-support.patch;
  };
  usb-typec-bus-Catch-crash-due-to-partner-NULL-value = {
    name = "usb-typec-bus-Catch-crash-due-to-partner-NULL-value";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-08-usb-typec-bus-Catch-crash-due-to-partner-NULL-value.patch;
  };
  usb-typec-tcpm-add-hacky-generic-altmode-support = {
    name = "usb-typec-tcpm-add-hacky-generic-altmode-support";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-09-usb-typec-tcpm-add-hacky-generic-altmode-support.patch;
  };
  phy-rockchip-typec-Set-extcon-capabilities = {
    name = "phy-rockchip-typec-Set-extcon-capabilities";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-10-phy-rockchip-typec-Set-extcon-capabilities.patch;
  };
  usb-typec-altmodes-displayport-Add-hacky-generic-alt = {
    name = "usb-typec-altmodes-displayport-Add-hacky-generic-alt";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-11-usb-typec-altmodes-displayport-Add-hacky-generic-alt.patch;
  };
  sound-soc-codecs-es8316-Run-micdetect-only-if-jack-s = {
    name = "sound-soc-codecs-es8316-Run-micdetect-only-if-jack-s";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-12-sound-soc-codecs-es8316-Run-micdetect-only-if-jack-s.patch;
  };
  ASoC-soc-jack-c-supported-inverted-jack-detect-GPIOs = {
    name = "ASoC-soc-jack.c-supported-inverted-jack-detect-GPIOs";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-13-ASoC-soc-jack.c-supported-inverted-jack-detect-GPIOs.patch;
  };
  arm64-dts-rockchip-add-default-rk3399-rockchip-suspe = {
    name = "arm64-dts-rockchip-add-default-rk3399-rockchip-suspe";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-14-arm64-dts-rockchip-add-default-rk3399-rockchip-suspe.patch;
  };
  arm64-dts-rockchip-enable-earlycon = {
    name = "arm64-dts-rockchip-enable-earlycon";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-15-arm64-dts-rockchip-enable-earlycon.patch;
  };
  arm64-dts-rockchip-reserve-memory-for-ATF-rockchip-S = {
    name = "arm64-dts-rockchip-reserve-memory-for-ATF-rockchip-S";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-16-arm64-dts-rockchip-reserve-memory-for-ATF-rockchip-S.patch;
  };
  arm64-dts-rockchip-use-power-led-for-disk-activity-i = {
    name = "arm64-dts-rockchip-use-power-led-for-disk-activity-i";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-17-arm64-dts-rockchip-use-power-led-for-disk-activity-i.patch;
  };
  arm64-dts-rockchip-add-typec-extcon-hack = {
    name = "arm64-dts-rockchip-add-typec-extcon-hack";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-18-arm64-dts-rockchip-add-typec-extcon-hack.patch;
  };
  arm64-dts-rockchip-add-rockchip-suspend-node = {
    name = "arm64-dts-rockchip-add-rockchip-suspend-node";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-19-arm64-dts-rockchip-add-rockchip-suspend-node.patch;
  };
  arm64-configs-add-defconfig-for-Pinebook-Pro = {
    name = "arm64-configs-add-defconfig-for-Pinebook-Pro";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-20-arm64-configs-add-defconfig-for-Pinebook-Pro.patch;
  };
  arm64-dts-rockchip-setup-USB-type-c-port-as-dual-dat = {
    name = "arm64-dts-rockchip-setup-USB-type-c-port-as-dual-dat";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-21-arm64-dts-rockchip-setup-USB-type-c-port-as-dual-dat.patch;
  };
  soc-rockchip-Port-rockchip_pm_config-driver-to-Linux = {
    name = "soc-rockchip-Port-rockchip_pm_config-driver-to-Linux";
    patch = ./pkgs/linux-pinebookPro/linux-libre-pinebook-pro-22-soc-rockchip-Port-rockchip_pm_config-driver-to-Linux.patch;
  };
  support-for-Pinebook-Pro = {
    name = "support-for-Pinebook-Pro";
    patch = ./pkgs/linux-pinebookPro/linux-libre-support-for-Pinebook-Pro.patch;
  };

  linux_pinebookPro = self.callPackage pkgs/linux-pinebookPro {
    kernelPatches = [
      self.kernelPatches.bridge_stp_helper
      self.soc-rockchip-Add-rockchip-suspend-mode-driver
      self.firmware-Add-Rockchip-SIP-driver
      self.tty-serdev-support-shutdown-op
      self.bluetooth-hci_serdev-Clear-registered-bit-on-unregis
      self.bluetooth-hci_bcm-disable-power-on-shutdown
      self.mmc-core-pwrseq_simple-disable-mmc-power-on-shutdown
      self.regulator-core-add-generic-suspend-states-support
      self.usb-typec-bus-Catch-crash-due-to-partner-NULL-value
      self.usb-typec-tcpm-add-hacky-generic-altmode-support
      self.phy-rockchip-typec-Set-extcon-capabilities
      self.usb-typec-altmodes-displayport-Add-hacky-generic-alt
      self.sound-soc-codecs-es8316-Run-micdetect-only-if-jack-s
      self.ASoC-soc-jack-c-supported-inverted-jack-detect-GPIOs
      self.arm64-dts-rockchip-add-default-rk3399-rockchip-suspe
      self.arm64-dts-rockchip-enable-earlycon
      self.arm64-dts-rockchip-reserve-memory-for-ATF-rockchip-S
      self.arm64-dts-rockchip-use-power-led-for-disk-activity-i
      self.arm64-dts-rockchip-add-typec-extcon-hack
      self.arm64-dts-rockchip-add-rockchip-suspend-node
      self.arm64-configs-add-defconfig-for-Pinebook-Pro
      self.arm64-dts-rockchip-setup-USB-type-c-port-as-dual-dat
      self.soc-rockchip-Port-rockchip_pm_config-driver-to-Linux
      #self.support-for-Pinebook-Pro
    ];
  };

  linux_pbPro = self.callPackage pkgs/kernel-latest { kernelPatches = []; };

  #linuxPackages_pinebookPro = self.linuxPackagesFor self.linux_pinebookPro;
  linuxPackages_pinebookPro = self.linuxPackagesFor self.linux_pbPro;
}

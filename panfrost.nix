self: super:
{
  mesa =
    let
      minVersion = "21.0.0";
    in
    if (self.lib.versionOlder super.mesa.version minVersion) then (super.mesa.overrideAttrs (o:
    let
      version = minVersion;
      branch  = builtins.head (self.lib.splitString "." version);
    in
    {
      inherit version;
      src =  super.fetchurl {
        urls = [
          "ftp://ftp.freedesktop.org/pub/mesa/mesa-${version}.tar.xz"
          "ftp://ftp.freedesktop.org/pub/mesa/${version}/mesa-${version}.tar.xz"
          "ftp://ftp.freedesktop.org/pub/mesa/older-versions/${branch}.x/${version}/mesa-${version}.tar.xz"
          "https://mesa.freedesktop.org/archive/mesa-${version}.tar.xz"
        ];
        sha256 = "110d5r9bl9nxl2680730yd273vz3v2fzjd2xvkwprmx8wsc4w876";
      };
      patches = [];
      postFixup = builtins.replaceStrings [
        "rm $dev/lib/pkgconfig/{gl,egl}.pc"
      ] [
        "rm $dev/lib/pkgconfig/gl.pc"
      ] o.postFixup;
    })) else super.mesa;
}

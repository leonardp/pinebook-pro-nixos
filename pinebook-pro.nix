{
  nixpkgs.overlays = let
    kernel = import ./kernel.nix;
    firmware = import ./firmware.nix;
    panfrost = import ./panfrost.nix;
  in [
    kernel
    firmware
    #panfrost
  ];
}
